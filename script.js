var matrixBlock = document.getElementById("matrix");
var maxValue;
var minValue;
var genBut = document.getElementById("genBut");
var autoBut = document.getElementById('autoBut');
var w,h;
var table;
var i,j,k,g;
let fontSize;
var arr = new Array();

function matrixGen() {

    w = +document.getElementById("width").value;
    h = +document.getElementById("height").value;
    maxValue = document.getElementById("maxV").value;
    minValue = document.getElementById("minV").value;
    for(let i = 0; i < h; i++){
        arr[i] = new Array();
        for(let j = 0; j < w; j++){
            arr[i][j] = new Array();
            for(let k = 0; k < 2; k++) {
                arr[i][j][k] = randomInteger(minValue, maxValue);
            }
        }
    }

}

function matrix1dGen() {

    w = +document.getElementById("width").value;
    h = +document.getElementById("height").value;
    maxValue = document.getElementById("maxV").value;
    minValue = document.getElementById("minV").value;
    for(let i = 0; i < h; i++){
        arr[i] = new Array();
        for(let j = 0; j < w; j++){
            arr[i][j] = new Array();
            arr[i][j] = randomInteger(minValue, maxValue);
        }
    }

    console.table(arr);
}
function genTable2D() {
    matrixGen();
    if(matrixBlock.childNodes.length > 0)
        while (matrixBlock.childNodes.length!==0)
            matrixBlock.removeChild(matrixBlock.firstChild);

    table = document.createElement('table');
    var html = '<tbody>';
    for (let i = 0; i <= h; i++) {
        html += '<tr>';
        for (let j = 0; j <= w; j++) {
            html += '<td></td>';
        }
        table.innerHTML = html + '</tr></tbody>';
    }
    matrixBlock.appendChild(table);
    for( i = 1; i <= h; i++) {
        for (j = 1; j <= w; j++) {
            for (k = 0; k < 2; k++) {
                table.rows[i].cells[j].innerHTML += arr[i - 1][j - 1][k] + '; ';
            }
        }
    }

    for(let i = 1; i <= h; i++) {
        table.rows[i].cells[0].innerHTML = "B" + i;
    }
    for(let i = 1; i <= w; i++) {
        table.rows[0].cells[i].innerHTML = "A" + i;
    }
}

function create2dMatrix() {
    if(matrixBlock.childNodes.length > 1)
        while (matrixBlock.childNodes.length!==0)
            matrixBlock.removeChild(matrixBlock.firstChild);

    table = document.createElement('table');
    var html = '<tbody>';
    for (let i = 0; i <= h; i++) {
        html += '<tr>';
        for (let j = 0; j <= w; j++) {
            html += '<td></td>';
        }
        table.innerHTML = html + '</tr></tbody>';
    }
    matrixBlock.appendChild(table);
    for( i = 1; i <= h; i++) {
        for (j = 1; j <= w; j++) {
            for (k = 0; k < 2; k++) {
                table.rows[i].cells[j].innerHTML += arr[i - 1][j - 1][k] + '; ';
            }
        }
    }

    for(let i = 1; i <= h; i++) {
        table.rows[i].cells[0].innerHTML = "B" + i;
    }
    for(let i = 1; i <= w; i++) {
        table.rows[0].cells[i].innerHTML = "A" + i;
    }
}

function genTable1D() {
    matrix1dGen();
    if(matrixBlock.childNodes.length > 0)
        while (matrixBlock.childNodes.length!==0)
            matrixBlock.removeChild(matrixBlock.firstChild);

    create1dMatrix();
    create1dMatrix();
}

function create1dMatrix() {
    table = document.createElement('table');
    var html = '<tbody>';
    for (let i = 0; i <= h; i++) {
        html += '<tr>';
        for (let j = 0; j <= w; j++) {
            html += '<td></td>';
        }
        table.innerHTML = html + '</tr></tbody>';
    }
    matrixBlock.appendChild(table);
    for( i = 1; i <= h; i++) {
        for (j = 1; j <= w; j++) {
            table.rows[i].cells[j].innerHTML += arr[i - 1][j - 1];

        }
    }

    for(let i = 1; i <= h; i++) {
        table.rows[i].cells[0].innerHTML = "<b>B</b>" + i;
        table.rows[i].cells[0].style.backgroundColor = '#00a66a'
    }
    for(let i = 1; i <= w; i++) {
        table.rows[0].cells[i].innerHTML = "<b>A</b>" + i;
        table.rows[0].cells[i].style.backgroundColor = '#00a66a'
    }
}

genBut.onclick = function () {
    if(document.getElementById('2d').checked){
        genTable1D();
    }
    if(document.getElementById('1d').checked){
        genTable2D();
    }
};

document.getElementById('changeStyle').addEventListener('click', function (event) {
    document.body.style.fontSize = document.getElementById('fontSize').value +'px';
    document.body.style.backgroundColor = document.getElementById('bColor').value;
    document.body.style.color = document.getElementById('fColor').value;
});


document.getElementById('matrix').addEventListener('click',function (event)  {
    if(event.target.nodeName === 'TD' && event.target.cellIndex!==0 && event.target.innerHTML[0] !=='A') {
        if (event.target.style.backgroundColor === '')
            event.target.style.backgroundColor = 'green';
        else if (event.target.style.backgroundColor === 'green')
            event.target.style.backgroundColor = 'red';
        else if (event.target.style.backgroundColor === 'red')
            event.target.style.backgroundColor = 'yellow'
        else if (event.target.style.backgroundColor === 'yellow')
            event.target.style.backgroundColor = ''
    }
});
document.oncontextmenu = function (){return false};

document.getElementById('matrix').addEventListener('contextmenu',function (event){
    event.target.style.backgroundColor = '';
});

autoBut.onclick = function () {
    if(document.getElementById('2d').checked){
        autoSearch();
    }
    if(document.getElementById('1d').checked){
        autoSearch2d();
    }
};

function autoSearch() {
        create1dMatrix();
        let max;
        for(i = 1; i <= h; i++){
            max = getMaxOfArray(arr[i-1]);
            for(j = 1; j <= w; j++) {
                if(table.rows[i].cells[j].innerHTML===max.toString()){
                    table.rows[i].cells[j].style.backgroundColor = 'green';
                }
            }
        }

        create1dMatrix();
    let col = [];
    for(i = 1; i <= h; i++){

        for(let g = 0; g< h; g++)
            col[g] = arr[g][i-1];
        max = getMaxOfArray(col);


        for(j = 1; j <= w; j++) {
            if(table.rows[j].cells[i].innerHTML===max.toString()){
                table.rows[j].cells[i].style.backgroundColor = 'green';
            }
        }
    }

}

function autoSearch2d() {
    create2dMatrix();
    let max;
    for(i = 1; i <= h; i++){
        max = getMaxOfMatrix(arr[i-1]);
        for(j = 1; j <= w; j++) {
            if(arr[i-1][j-1][1]===max){
                    if( table.rows[i].cells[j].style.backgroundColor === '')
                        table.rows[i].cells[j].style.backgroundColor = 'green';
                    else if( table.rows[i].cells[j].style.backgroundColor === 'green')
                        table.rows[i].cells[j].style.backgroundColor = 'red';
                    else if( table.rows[i].cells[j].style.backgroundColor === 'red')
                        table.rows[i].cells[j].style.backgroundColor = 'yellow'
            }
        }
    }
    for(i = 1; i <= h; i++) {
        let col = [];
         for(let g = 0; g< h; g++)
            col[g] = arr[g][i-1][0];

         console.table(col);
        max = getMaxOfArray(col);
        console.log(max);
        for (j = 1; j <= w; j++) {
            if(arr[j-1][i-1][0] === max){
                //table.rows[j].cells[i].style.backgroundColor = 'green';
                if( table.rows[j].cells[i].style.backgroundColor === '')
                    table.rows[j].cells[i].style.backgroundColor = 'green';
                else if( table.rows[j].cells[i].style.backgroundColor === 'green')
                    table.rows[j].cells[i].style.backgroundColor = 'red';
                else if( table.rows[j].cells[i].style.backgroundColor === 'red')
                    table.rows[j].cells[i].style.backgroundColor = 'yellow';
            }
        }
    }


}




function getMaxOfArray(numArray) {
    return Math.max.apply(null, numArray);
}
function getMaxOfMatrix(numArray) {
    let max, row = new Array();
    for( let i = 0; i<numArray.length; i++){
        row[i] = numArray[i][1];
    }
    max = getMaxOfArray(row);
    return max;
}
function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

window.onload = function () {
    matrixGen();
}